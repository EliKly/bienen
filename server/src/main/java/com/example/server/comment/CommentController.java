package com.example.server.comment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {

    @Autowired
    CommentService commentService;
    @Autowired
    CommentRepository commentRepository;


    /*@GetMapping(value = "/getComments")
    public List<Comment> getAds() {
        return commentService.getComments();
    }*/

    @PostMapping(value= "/saveComment")
    public void saveCategory(@RequestBody CommentDTO commentDTO){
        commentService.saveComment(commentDTO);

    }
}
