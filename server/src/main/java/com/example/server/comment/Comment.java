package com.example.server.comment;

import com.example.server.ad.Ad;
import com.example.server.user.User;
import lombok.Data;

import javax.persistence.*;
//import java.sql.Date;
import java.time.LocalDate;
import java.util.Date;
@Data
@Entity
public class Comment {
    @Id
    @GeneratedValue
    private Long id;
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    private Date zeitstempel;

    @ManyToOne(optional = true)
    private User user;

    @ManyToOne(optional = true)
    private Ad ad;

    public Comment(){}

    public Comment(String text) {
        this.text = text;
    }

    public Comment(String text, Ad ad) {
        this.text = text;
        this.ad = ad;
    }

    public Comment(String text, User user) {
        this.text = text;
        this.user = user;
    }

    public Comment(String text, User user, Ad ad) {
        this.text = text;
        this.user = user;
        this.ad = ad;
    }

   /* public String getText() { return text; }

    public void setText(String text) { this.text = text; }

    public void setUser (User user){this.user = user;}*/
}

