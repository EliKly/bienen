package com.example.server.comment;

import com.example.server.ad.Ad;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Collection;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
  List<Comment> findByAd_Id(Ad ad);
}