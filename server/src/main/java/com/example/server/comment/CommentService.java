package com.example.server.comment;

import com.example.server.ad.Ad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    public void saveComment(CommentDTO commentDTO) {
        Comment c = new Comment(commentDTO.getText(), commentDTO.getUser(), commentDTO.getAd());
        commentRepository.save(c);
    }

    public List<Comment> getComments(Ad ad) {
        return commentRepository.findByAd_Id(ad);
    }
}
