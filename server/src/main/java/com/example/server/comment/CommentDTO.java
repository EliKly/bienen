package com.example.server.comment;

import com.example.server.ad.Ad;
import com.example.server.user.User;

public class CommentDTO {

    private String text;
    private Ad ad;
    private User user;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Ad getAd() {
        return ad;
    }

    public void setAd(Ad ad) {
        this.ad = ad;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
