package com.example.server.ad;

import com.example.server.categ.Categ;
import com.example.server.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AdController {

    @Autowired
    AdService adService;
    @Autowired
    AdRepository adRepository;


    @GetMapping(value = "/getAllAds")
    public List<Ad> getAllAds() {
        return adService.getAllAds();
    }

    @GetMapping(value = "/getMyAds")
    public List<Ad> getMyAds(@RequestBody User user) {
        return adService.getMyAds(user);
    }

    @GetMapping(value = "/getThreeAds")
    public List<Ad> getThreeAds() {
        return adService.getTreeAds();
    }

    @GetMapping(value = "/getAdsWithCateg")
    public List<Ad> getAdsWithCateg(@RequestBody Categ caterorie ) {
        return adService.getAdsWithCateg(caterorie);
    }

    @PostMapping(value = "/saveAd")
    public void saveAd(@RequestBody AdDTO adDTO) {
        adService.saveAd(adDTO);
    }
}
