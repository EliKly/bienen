package com.example.server.ad;

import com.example.server.categ.Categ;
import com.example.server.user.User;

public class AdDTO {
    private String headline;
    private String text;
    private Categ categorie;
    private User user;


    public String getHeadline() { return headline;}

    public void setHeadline(String headline) {this.headline = headline;}

    public Categ getCategorie() {
        return categorie;
    }

    public void setCategorie(Categ categorie) {
        this.categorie = categorie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
