package com.example.server.ad;

import com.example.server.categ.Categ;
import com.example.server.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdService {
    @Autowired
    AdRepository adRepository;


    public void saveAd(AdDTO adDTO) {
        Ad a = new Ad(adDTO.getHeadline(),adDTO.getText(),adDTO.getUser(),adDTO.getCategorie());
        adRepository.save(a);
    }


    public List<Ad> getAllAds() {
        return adRepository.findAll();
    }

    public List<Ad> getMyAds(User user) { return adRepository.findByUser(user); }

    public List <Ad> getTreeAds() {
        List<Ad> allAds = new ArrayList<>();
        allAds = adRepository.findAll();
        List<Ad> threeAds = new ArrayList<>();
        try {
            threeAds.add(allAds.get(allAds.size()-1));
            threeAds.add(allAds.get(allAds.size()-2));
            threeAds.add(allAds.get(allAds.size()-3));

        }
        catch (Exception e){
            System.out.println("adkjfökdjövaekjrbgpöadkj v");
        }
        return threeAds; }

    public List <Ad> getAdsWithCateg(Categ categorie) {return adRepository.findByCategorie(categorie);}
}
