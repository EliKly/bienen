package com.example.server.ad;

import com.example.server.categ.Categ;
import com.example.server.comment.Comment;
import com.example.server.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdRepository extends JpaRepository<Ad, Long> {
    List<Ad> findByUser(User user);

   @Query(value = "SELECT text FROM ad ORDER BY zeitstempel DESC LIMIT 3",
            nativeQuery = true)
    List<Ad> getThreeAds();


    List<Ad> findAll();

    List<Ad> findByCategorie(Categ categorie);
}
