package com.example.server.ad;


import com.example.server.categ.Categ;
import com.example.server.comment.Comment;
import com.example.server.user.User;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
//import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

@Data
@Entity
public class Ad {
    @Id
    @GeneratedValue
    private Long id;

    private String headline;

    public Ad(String headline, String text, User user, Categ categorie) {
        this.headline = headline;
        this.text = text;
        this.zeitstempel = zeitstempel;
        this.comments = comments;
        this.user = user;
        this.categorie = categorie;
    }

    @Lob
    private String text;

    /*@Temporal(TemporalType.TIMESTAMP)
    private Date zeitstempel;*/

    @CreationTimestamp
    private LocalDate zeitstempel;

    @OneToMany
    private Collection<Comment> comments;

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private Categ categorie;

    public Ad(String text) {
        this.text = text;
    }

    public Ad(String text, Collection<Comment> comments) {
        this.text = text;
        this.comments = comments;
    }

    public Ad(String text, Collection<Comment> comments, User user) {
        this.text = text;
        this.comments = comments;
        this.user = user;
    }

    public Ad(String text, Collection<Comment> comments, Categ categorie, User user) {
        this.text = text;
        this.comments = comments;
        this.categorie = categorie;
        this.user = user;
    }

    public Ad(String text, Categ categorie, User user) {
        this.text = text;
        this.categorie = categorie;
        this.user = user;
    }

    public Ad() {
    }

    public Ad(String text, Categ categorie) {
        this.text = text;
        this.categorie = categorie;
    }
    public Ad(String headline, String text, Categ categorie) {
        this.headline = headline;
        this.text = text;
        this.categorie = categorie;
    }

/*    public Collection<Comment> getComments() {
        return comments;
    }

    public void setComments(Collection<Comment> comments) {
        this.comments = comments;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }*/
}
