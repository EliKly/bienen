package com.example.server.user;

import com.example.server.ad.Ad;
import com.example.server.comment.Comment;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;
import java.util.UUID;

import lombok.Data;
import com.example.server.comment.Comment;


@Data
@Entity
public class User {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type = "uuid-char")
    private UUID id;

    private String username;
    private String password;
    private String status;

    @OneToMany(mappedBy = "user")
    private Collection<Ad> ads;

    @OneToMany(mappedBy = "user")
    private Collection<Comment> comments;


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
        comment.setUser(this);
    }

    public void removeComment(Comment comment) {
        this.comments.remove(comment);
    }

    public void addAd(Ad ad) {
        this.ads.add(ad);
        ad.setUser(this);
    }

    public void removeAd(Ad ad) {
        this.ads.remove(ad);
    }
}
