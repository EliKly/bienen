package com.example.server.greeting;

import com.example.server.api.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class GreetingController {

    @Autowired
    private GreetingRepository greetingRepository;

    @Autowired
    private SecurityService securityService;

    @GetMapping("/greeting")
    public List<Greeting> getAll() {
        return this.greetingRepository.findAll();
    }

    @PostMapping("/greeting")
    public  Greeting create(@RequestBody GreetingDTO greetingDTO) {
        Greeting greeting = new Greeting();
        greeting.setText(greetingDTO.text);
        greeting.setUser(securityService.getSessionUser().orElse(null));
        return greetingRepository.save(greeting);
    }
}
