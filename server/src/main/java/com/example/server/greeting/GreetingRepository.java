package com.example.server.greeting;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface GreetingRepository extends JpaRepository<Greeting, UUID> {
}
