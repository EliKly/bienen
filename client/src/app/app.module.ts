import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { LoginFormComponent } from './security/login-form/login-form.component';
import { UserInfoComponent } from './security/user-info/user-info.component';
import { GreetingCreateComponent } from './greeting/greeting-create/greeting-create.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from './registration/registration.component';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { StartpageComponent } from './startpage/startpage.component';
import { AppRoutingModule } from "./app-routing.module";
import { AdComponent } from './ad/ad.component';
import { HeadpageComponent } from './headpage/headpage.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    UserInfoComponent,
    GreetingCreateComponent,
    RegistrationComponent,
    StartpageComponent,
    AdComponent,
    HeadpageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    NgbModule.forRoot(),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
