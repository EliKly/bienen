import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {StartpageComponent} from "./startpage/startpage.component";
import {HeadpageComponent} from "./headpage/headpage.component";

const routes: Routes = [
  { path: '', redirectTo: '/startpage', pathMatch: 'full' },
  { path: 'startpage', component: StartpageComponent },
  { path: 'headpage', component: HeadpageComponent}
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
