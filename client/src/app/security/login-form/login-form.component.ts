import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../security.service';
import { HttpClient } from '@angular/common/http';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  username = '';
  password = '';
  error: string|null = null;

  ngOnInit() {
  }

  constructor(private security: SecurityService, private http: HttpClient, private router: Router) {
  }

  login() {
    this.security.login(this.username, this.password).subscribe({
      complete: () => {this.error = null, this.router.navigate(['headpage'])},
      error: err => this.error = err
    });
  }

}
