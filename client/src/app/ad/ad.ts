import {User} from "../security/user";
import {Categ} from "../Categ";

export interface Ad {
  headline:string;
  text:string;
  user:User
  categorie: Categ;
}

