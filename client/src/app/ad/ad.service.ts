import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {Ad} from "./ad";
import {Observable} from "rxjs";
import {Categ} from "../Categ";

@Injectable({
  providedIn: 'root'
})
export class AdService {

  constructor(private http: HttpClient) {
  }

  public getTreeAds(): Observable<Ad[]> {
    return this.http.get<Ad[]>('api/getThreeAds')
  }

  public getAdsWithCateg(categorie: Categ): Observable<Ad[]> {
    return this.http.get<Ad[]>('api/getAdsWithCateg')
  }
}
