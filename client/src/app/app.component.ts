import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GreetingService } from './greeting/greeting.service';
import { Greeting } from './greeting/greeting';
import { SecurityService } from './security/security.service';
import { User } from './security/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'client';
  greetings: Greeting[]|null = null;
  user: User|null = null;

  constructor(private greetingService: GreetingService, private security: SecurityService) {
  }

  ngOnInit(): void {
    this.greetingService.getGreetings().subscribe(
      greetings => this.greetings = greetings
    );
    this.security.getUser().subscribe(
      user => this.user = user
    );
  }
  logout() {
    this.security.logout();

  }
}
