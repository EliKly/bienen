import {UserRegister} from "./user-register";
import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) {
  }

  public registerCustomer(user: UserRegister) {
    return this.http.post('/api/registration', user);
  }
}
