import {Component, OnInit} from '@angular/core';
import {UserRegister} from "./user-register";
import {delay} from "q";
import {RegisterService} from "./register.service";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  error: string | null = null;

  complete: boolean = false;

  user: UserRegister = {
    username: '',
    password: '',
  };

  constructor(private registerService: RegisterService) {
  }

  register() {

    this.complete = true;

    this.registerService.registerCustomer(this.user).subscribe({
      complete: () => this.error = null,
      error: async err => {
        await delay(6000);
        this.error = err;
        this.complete = false
      },
    })

    this.resetForm();
  }

  ngOnInit() {
  }

  resetForm() {
    this.user.username = '';
    this.user.password = '';
  }
}

